# sgw.py

import os
from datetime import datetime, timedelta
import uvloop
import asyncio
import aiohttp
from sanic import Sanic, response
from sanic.response import json
from sanic_cors import CORS, cross_origin
import json as pjson
from googleapiclient.discovery import build

# logging #{{{
import logging
logger = logging.getLogger('sgw')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(asctime)s: %(name)s] %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)
#}}}

# event loop
loop = uvloop.new_event_loop()
app = Sanic()
CORS(app)

G_API_KEY = os.environ.get('G_API_KEY', 'AIzaSyCgRS_W4tjrKZVfL1sbYfO6tRL73K2m9ns')
G_CSE = os.environ.get('G_CSE', '012157162512915970965:qpbcxkigduu')

gservice = build("customsearch", "v1", developerKey=G_API_KEY)

@app.listener('before_server_start')
async def init(app, loop):
    logger.info('starting...')
    pass

@app.listener('after_server_stop')
async def finish(app, loop):
    logger.info('stoping...')
    pass

def g_cse_search(search_term):
    return gservice.cse().list(q=search_term, cx=G_CSE, num=10, start=1).execute()

async def gsearch(kwd):
    loop = asyncio.get_event_loop()
    res = await loop.run_in_executor(None, g_cse_search, kwd)
    return res

@app.route("/v1/search", methods=['GET'])
async def search(request):
    # request
    params = request.args
    kwd = params.get('kwd', None)
    logger.info('kwd: %s' % kwd)
    if kwd:
        #results = await asyncio.gather(\
        #            gsearch(kwd), \
        #            return_exceptions=True
        #        )
        #logger.info("---- %s" % results)
        results = await gsearch(kwd)
        return response.json({
            'words': kwd,
            'google': results, 
        })
    else:
        ret = {
                'words': '',
                'status': 'error',
                'message': 'search word is None'
        }
        return ret


if __name__ == "__main__":
    import sys
    print(sys.argv)
    if len(sys.argv) < 2:
        PORT = 8000
    else:
        PORT = sys.argv[1]
    app.run(host="0.0.0.0", port=PORT, worker=4, debug=True)

