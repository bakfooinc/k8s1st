FROM continuumio/miniconda3 

COPY . .

RUN  apt-get update -y && \
     apt-get install -y git-core build-essential wget vim redis-tools

RUN  conda install -c anaconda psycopg2 && \
     pip install -r requirements.txt

CMD ["python", "k8s1st.py"]
EXPOSE 8000 
